# This is just Redux tutorial

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Useful Resources & Links
- [ReactJS](https://reactjs.org/)
- [Redux](https://redux.js.org/)
  - [Redux Async Logic](https://redux.js.org/tutorials/fundamentals/part-6-async-logic)
- [redux-thunk](https://github.com/reduxjs/redux-thunk)
- [Redux DevTools](https://github.com/zalmoxisus/redux-devtools-extension)

### `npm install`
### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### Where to put the logic ?
| Action                                      | Reducer                                       |
| ------------------------------------------- | --------------------------------------------- |
| Can run Async Code                          | Pure, Sync Code Only!                         |
| Shouldn't prepare the State Update Too Much | Core Redux Concept: Reducers update the State |

